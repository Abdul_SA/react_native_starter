#### Configure Eslint
* Add a custom file watcher
* File type: Any
* Scope: Project files
* Program: ./node_modules/eslint/bin/eslint.js 
* Arguments: --fix $FilePath$
* Output paths to refresh: $FileDir$
* Expand advanced options
* Uncheck `Auto-save edited files to trigger the watcher`
* Uncheck `Trigger the watcher on external changes`
