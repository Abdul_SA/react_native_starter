import { Dispatch, Middleware } from 'redux';

import { Actions } from '@/actions';
import { State } from '@/reducers';
import { Listeners } from '@types';

export const createListeners = (
  listeners: Listeners
): Middleware<Dispatch<Actions>, State> => store => next => action => {
  // listeners are provided with a picture
  // of the world before the action is applied
  const preActionState = store.getState();

  // release the action to reducers before
  // firing additional actions
  next(action);

  // always async
  setTimeout(() => {
    // can have multiple listeners listening
    // against the same action.type
    if (listeners[action.type]) {
      listeners[action.type](action, store.dispatch, preActionState);
    }
  });
};
