import { applyMiddleware, createStore, Middleware, Store as ReduxStore } from 'redux';
import logger from 'redux-logger';

import { Actions } from '@/actions';
import { listeners } from '@/listeners';
import { rootReducer, State } from '@/reducers';
import { createListeners } from '@/store/middleware/createListeners';

const initialState = {};

let middleware: Middleware[] = [createListeners(listeners)];

if (process.env.NODE_ENV === 'development') {
  // middleware applied in debug mode only
  middleware = [...middleware, logger];
}

export type Store = ReduxStore<State, Actions>;

export const configureStore = (): Store =>
  createStore(rootReducer, initialState, applyMiddleware(...middleware));
