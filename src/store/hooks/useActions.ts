import { useMemo } from 'react';
import { useDispatch } from 'react-redux';
import { ActionCreatorsMapObject, bindActionCreators } from 'redux';

export const useActions = <T extends ActionCreatorsMapObject>(actions: T) => {
  const dispatch = useDispatch();
  /*
   * we're assuming actions don't change between re-renders, so it's not part of deps
   */
  /* eslint-disable react-hooks/exhaustive-deps */
  return useMemo(() => bindActionCreators(actions, dispatch), [dispatch]);
};
