import { Dispatch } from 'redux';

import { Actions } from '@/actions';
import { FetchDataAction, fetchDataError, fetchDataSuccess } from '@/actions/fetchData';
import { State } from '@/reducers';

export const onFetchData = (action: FetchDataAction, dispatch: Dispatch<Actions>, _: State) => {
  try {
    /*
     * make any async requests here
     */
    dispatch(fetchDataSuccess('Data was fetched!'));
  } catch (e) {
    dispatch(fetchDataError(e));
  }
};
