import { ActionType } from '@/actions/ActionType';
import { Listeners } from '@types';

import { onFetchData } from './onFetchData';

export const listeners = {
  [ActionType.FetchDataRequest]: onFetchData,
} as Listeners;
