import React from 'react';
import { Options } from 'react-native-navigation';

import HomeFrame from '@/components/HomeFrame';

export class HomeScreen extends React.Component {
  public static navID = 'HomeScreen';

  public static options(): Options {
    return {
      topBar: {
        title: {
          text: 'Home',
        },
      },
    };
  }

  public render() {
    return <HomeFrame />;
  }
}
