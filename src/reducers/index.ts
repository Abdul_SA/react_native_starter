import { combineReducers } from 'redux';

import { Actions } from '@/actions';

import { data, State as DataState } from './data';

export interface State {
  data: DataState;
}

export const rootReducer = combineReducers<State, Actions>({
  data,
});
