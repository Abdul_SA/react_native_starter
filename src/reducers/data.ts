import produce from 'immer';

import { Actions } from '@/actions';
import { ActionType } from '@/actions/ActionType';

export interface State {
  isLoading: boolean;
  value: Opt<string>;
}

const defaultState: State = {
  isLoading: false,
  value: undefined,
};

export const data = (state = defaultState, action: Actions): State => {
  switch (action.type) {
    case ActionType.FetchDataRequest:
      return produce(state, draftState => {
        draftState.isLoading = true;
      });
    case ActionType.FetchDataSuccess:
      return produce(state, draftState => {
        draftState.isLoading = false;
        draftState.value = action.payload.data;
      });
    case ActionType.FetchDataError:
      return produce(state, draftState => {
        draftState.isLoading = false;
      });
    default:
      return state;
  }
};
