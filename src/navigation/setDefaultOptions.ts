import { Navigation } from 'react-native-navigation';

export const setDefaultOptions = () =>
  Navigation.setDefaultOptions({
    layout: {
      componentBackgroundColor: '#fff',
      orientation: ['portrait'],
    },
    bottomTabs: {
      titleDisplayMode: 'alwaysShow',
    },
    bottomTab: {
      selectedIconColor: '#5847ff',
      selectedTextColor: '#5847ff',
    },
  });
