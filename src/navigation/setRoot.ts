import { Navigation } from 'react-native-navigation';

import { HomeScreen } from '@/screens';

export const setRoot = () =>
  Navigation.setRoot({
    root: {
      bottomTabs: {
        children: [
          {
            stack: {
              children: [
                {
                  component: {
                    name: HomeScreen.navID,
                  },
                },
              ],
              options: {
                bottomTab: {
                  text: 'Home',
                },
              },
            },
          },
          {
            stack: {
              children: [
                {
                  component: {
                    name: HomeScreen.navID, // reuse same screen
                  },
                },
              ],
              options: {
                bottomTab: {
                  text: 'About',
                },
              },
            },
          },
        ],
      },
    },
  });
