import { useContext, useMemo } from 'react';
import { Navigation, Options } from 'react-native-navigation';

import { NavContext } from '@/navigation/NavContext';
import { NavCompClass } from '@types';

export const useNav = () => {
  const { componentId } = useContext(NavContext);

  return useMemo(
    () => ({
      dismissAllModals: () => Navigation.dismissAllModals(),

      dismissModal: () => {
        return Navigation.dismissModal(componentId);
      },

      dismissOverlay: () => {
        return Navigation.dismissOverlay(componentId);
      },

      mergeOptions: (options: Options) => {
        return Navigation.mergeOptions(componentId, options);
      },

      pop: () => Navigation.pop(componentId),

      push: <P extends object>(CompClass: NavCompClass<P>, passProps: P, options?: Options) =>
        Navigation.push(componentId, {
          component: {
            name: CompClass.navID,
            options,
            passProps,
          },
        }),

      showModal: <P extends object>(
        CompClass: NavCompClass<P>,
        passProps: P,
        options?: Options
      ) => {
        return Navigation.showModal({
          stack: {
            children: [
              {
                component: {
                  name: CompClass.navID,
                  options,
                  passProps,
                },
              },
            ],
          },
        });
      },

      showOverlay: <P extends object>(
        CompClass: NavCompClass<P>,
        passProps: P,
        options?: Options
      ) => {
        return Navigation.showOverlay({
          component: {
            name: CompClass.navID,
            options,
            passProps,
          },
        });
      },
    }),
    [componentId]
  );
};
