import hoistNonReactStatics from 'hoist-non-react-statics';
import React from 'react';
import { Provider as PaperProvider } from 'react-native-paper';
import { Provider } from 'react-redux';

import { NavContext } from '@/navigation/NavContext';
import { configureStore } from '@/store/configureStore';

const store = configureStore();

interface NavigationProps {
  componentId: string;
}

export const withProvider = <P extends object = {}>(Comp: React.ComponentType<P>) => {
  const WrappedComp = (props: P & NavigationProps) => (
    <NavContext.Provider value={{ componentId: props.componentId }}>
      <Provider store={store}>
        <PaperProvider>
          <Comp {...props} />
        </PaperProvider>
      </Provider>
    </NavContext.Provider>
  );

  return hoistNonReactStatics(WrappedComp, Comp);
};
