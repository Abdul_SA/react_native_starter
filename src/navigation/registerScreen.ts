import { Navigation } from 'react-native-navigation';

import { HomeScreen } from '@/screens';

import { withProvider } from './withProvider';

const SCREENS = [HomeScreen];

export const registerScreens = () => {
  SCREENS.forEach(screen =>
    Navigation.registerComponent(screen.navID, () => withProvider(HomeScreen))
  );
};
