import { Navigation } from 'react-native-navigation';

import { registerScreens } from '@/navigation/registerScreen';
import { setDefaultOptions } from '@/navigation/setDefaultOptions';
import { setRoot } from '@/navigation/setRoot';

export const start = () => {
  registerScreens();
  Navigation.events().registerAppLaunchedListener(() => {
    setDefaultOptions();
    setRoot();
  });
};
