export enum ActionType {
  FetchDataRequest = 'FetchDataRequest',
  FetchDataSuccess = 'FetchDataSuccess',
  FetchDataError = 'FetchDataError',
}
