import { ActionType } from './ActionType';

export const fetchData = () =>
  <const>{
    type: ActionType.FetchDataRequest,
  };

export const fetchDataSuccess = (data: string) =>
  <const>{
    payload: { data },
    type: ActionType.FetchDataSuccess,
  };

export const fetchDataError = (error: Error) =>
  <const>{
    type: ActionType.FetchDataError,
    error: true,
    payload: error,
  };

export type FetchDataAction = ReturnType<typeof fetchData>;
export type FetchDataActionSuccess = ReturnType<typeof fetchDataSuccess>;
export type FetchDataActionError = ReturnType<typeof fetchDataError>;
