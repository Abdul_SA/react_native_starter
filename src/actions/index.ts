import { FetchDataAction, FetchDataActionError, FetchDataActionSuccess } from '@/actions/fetchData';

export type Actions = FetchDataAction | FetchDataActionSuccess | FetchDataActionError;
