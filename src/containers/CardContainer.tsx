import React, { memo, useEffect } from 'react';
import { useSelector } from 'react-redux';

import { fetchData } from '@/actions/fetchData';
import CardExample from '@/components/Card';
import { useNav } from '@/navigation/hooks/useNav';
import { State } from '@/reducers';
import { HomeScreen } from '@/screens';
import { useActions } from '@/store/hooks/useActions';

const CardContainer = memo(() => {
  const actions = useActions({ fetchData });
  const nav = useNav();

  /*
   * Subscribe to state fields
   */
  const { isLoading, data } = useSelector((state: State) => ({
    isLoading: state.data.isLoading,
    data: state.data.value,
  }));

  /*
   * fetch data on mount
   */
  useEffect(() => {
    actions.fetchData();
  }, [actions]);

  const handlePush = () => {
    nav.push(HomeScreen, {});
  };

  if (isLoading) {
    /*
     * return null or loading component
     */
    return null;
  }

  return <CardExample data={data} onPushPress={handlePush} />;
});

export default CardContainer;
