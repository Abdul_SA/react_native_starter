import React from 'react';
import { SafeAreaView, ScrollView } from 'react-native';

import CardContainer from '@/containers/CardContainer';

const HomeFrame = () => (
  <SafeAreaView>
    <ScrollView>
      <CardContainer />
      <CardContainer />
      <CardContainer />
    </ScrollView>
  </SafeAreaView>
);

export default HomeFrame;
