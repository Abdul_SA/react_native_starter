import React, { memo } from 'react';
import { StyleSheet } from 'react-native';
import { Button, Card, Paragraph } from 'react-native-paper';

import { images } from '@/assets/images';

interface Props {
  data?: string;
  onPushPress: () => void;
}

const CardExample = memo<Props>(({ data, onPushPress }) => (
  <Card style={styles.card}>
    <Card.Cover source={images.wreckedShip} />
    <Card.Title title="Abandoned Ship" />
    <Card.Content>
      <Paragraph>
        The Abandoned Ship is a wrecked ship located on Route 108 in Hoenn, originally being a ship
        named the S.S. Cactus. The second part of the ship can only be accessed by using Dive and
        contains the Scanner.
      </Paragraph>
      <Button onPress={onPushPress}>Push Home Screen</Button>
    </Card.Content>
  </Card>
));

const styles = StyleSheet.create({
  card: {
    margin: 4,
  },
});

export default CardExample;
