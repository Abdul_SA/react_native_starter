declare module '@types' {
  import { Dispatch } from 'redux';
  import { Actions } from '@/actions';
  import { State } from '@/reducers';
  import { ActionType } from '@/actions/ActionType';

  type Listeners = {
    [key in keyof typeof ActionType]: (
      action: Actions,
      dispatch: Dispatch<Actions>,
      state?: State
    ) => Promise<void>
  };

  interface NavCompClass<P, S = React.ComponentState> extends React.ComponentClass<P, S> {
    navID: string;
  }
}
