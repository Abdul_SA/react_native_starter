interface Dict {
  [key: string]: any;
}

type Opt<T> = T | undefined;
